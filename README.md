# Projeto Beat

## Contribuindo para o projeto

O projeto usa o Unity 2019.3.2f1 e é compativel com essa versão ou versões mais novas e é feito exclusivamente em C#.

O Unity pode ser adquirido gratuitamente pelo Unity Hub disponivel em: https://unity3d.com/pt/get-unity/download

Após o download do Unity e a clonagem do repositorio selecione o diretorio usando a opção "Adicionar".

As Cenas do projeto estão localizadas em : Assets/Scenes

## Releases

Build pronta para jogar disponivel em:
https://gitlab.com/projeto-de-desenvolvimento-2020.2/arthur-repo/jogo/-/releases