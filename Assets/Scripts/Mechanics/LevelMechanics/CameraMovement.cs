﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	private Transform transformV; //transform da camera
	private GameObject[] players;//procurar players
	private GameObject barreiraLeft;
	private GameObject barreiraRight;
	
	// Use this for initialization
	void Start () {
		barreiraLeft = GameObject.Find ("BarrierLeft");
		barreiraRight = GameObject.Find ("BarrierRight");
	}
	
	// Update is called once per frame
	void Update () {
		int nPlayer = 0; //numero de jogadores na partida
		float posicaox = 0; //soma posiçoes dos players e define posicao da camera

		players = GameObject.FindGameObjectsWithTag("Player");
		if(players.Length > 0)
        {
			foreach (GameObject player in players)
			{
				posicaox = posicaox + player.transform.position.x;
				nPlayer++;
			}
			posicaox = posicaox / nPlayer;

			float distancia1 = Mathf.Abs(posicaox - barreiraLeft.transform.position.x);
			float distancia2 = Mathf.Abs(posicaox - barreiraRight.transform.position.x);

			if (distancia1 > 8.865 && distancia2 > 8.865)
			//if (Mathf.Abs(transformV.position.x - barreira1.transform.position.x) < 8.865 || Mathf.Abs(transformV.position.x - barreira2.transform.position.x) < 8.865)
			{
				transform.position = new Vector3(posicaox, transform.position.y, transform.position.z);
			}
		}
		
	}
}
