﻿using UnityEngine;
using System.Collections;

public class CharacterHitReceive : MonoBehaviour {
    public BaseMain basemain;
	public Animator animator;
    [HideInInspector] public Vector3 KnockBackPass; //vector 3 used to knockback in certain direction
    [HideInInspector] public float RunHitKnockBack = 5;

    protected int baseDamage = 100;

    //DragonPunch
    protected Vector3 DragonPunchSpecialUpAtkHit = new Vector3 (3,7,0);
    //Explosion
    protected Vector3 ExplosionSpecialUpAtkHit = new Vector3 (4,5,0);
    //Knight
    protected Vector3 KnightSpecial = new Vector3(7, 6, 0);
    protected Vector3 KnightHit3 = new Vector3(3,3,0);

    //Minotaur
    protected Vector3 MinotaurHit1 = new Vector3(5,5,0);

    protected Vector3 GolemHit1 = new Vector3(6,6,0);
    protected Vector3 GolemSpecialEffect = new Vector3(6,6,0);
    //Skeleton
    protected Vector3 SkeletonHit2 = new Vector3(3,3,0);

    void OnTriggerEnter(Collider Hit)
    {
        int bonusDamage = 0;

        CharacterController enemycontroller;
        BaseMain enemyBaseMain;
        if (Hit.gameObject.transform.parent == null || Hit.gameObject.transform.parent.gameObject.GetComponent<CharacterController>() == null) //reserved for projectiles
        {
            if (Hit.gameObject.name == "GolemSpecial(Clone)")
            {
                bonusDamage = 100;
                KnockBack(new Vector3(GolemSpecialEffect.x, GolemSpecialEffect.y, GolemSpecialEffect.z), Hit);
            }
            else
            {
                animator.SetBool("BeingHit", true);
            }
        }
        else
        {
            enemycontroller = Hit.gameObject.transform.parent.gameObject.GetComponent<CharacterController>();
            enemyBaseMain = Hit.gameObject.transform.parent.gameObject.GetComponent<BaseMain>();

            if (!basemain.specialStatus.armored)
            {
                //Explosion

                //Explosion Run Hit
                if (Hit.gameObject.name == "ExplosionRunHit")
                {
                    //Debug.Log("yup that sure is ExplosionRunHit");
                    KnockBack(new Vector3(RunHitKnockBack, 5, enemycontroller.velocity.z), Hit);
                }
                //Explosion Special Up Atk
                else if (Hit.gameObject.name == "ExplosionSpecialUpAtkHit")
                {
                    KnockBack(new Vector3(ExplosionSpecialUpAtkHit.x, ExplosionSpecialUpAtkHit.y, ExplosionSpecialUpAtkHit.z), Hit);
                }

                //DragonPunch

                //DragonPunch Run Hit
                else if (Hit.gameObject.name == "DragonPunchRunHit")
                {
                    KnockBack(new Vector3(RunHitKnockBack, 5, enemycontroller.velocity.z), Hit);
                }
                //DragonPunch Special Up Atk
                else if (Hit.gameObject.name == "DragonPunchSpecialUpAtkHit")
                {
                    KnockBack(new Vector3(DragonPunchSpecialUpAtkHit.x, DragonPunchSpecialUpAtkHit.y, DragonPunchSpecialUpAtkHit.z), Hit);
                }

                //Knight

                //Knight Run Hit
                else if (Hit.gameObject.name == "KnightHit3" || Hit.gameObject.name == "RogueHit3")
                {
                    KnockBack(new Vector3(KnightHit3.x, KnightHit3.y, KnightHit3.z), Hit);
                }
                else if (Hit.gameObject.name == "KnightRunHit" || Hit.gameObject.name == "RogueRunHit")
                {
                    KnockBack(new Vector3(RunHitKnockBack, 5, enemycontroller.velocity.z), Hit);
                }
                else if (Hit.gameObject.name == "KnightSpecialHit")
                {
                    KnockBack(new Vector3(KnightSpecial.x, KnightSpecial.y, KnightSpecial.z), Hit);
                }

                //Minotaur

                else if (Hit.gameObject.name == "MinotaurHit1")
                {
                    KnockBack(new Vector3(MinotaurHit1.x, MinotaurHit1.y, MinotaurHit1.z), Hit);
                }

                //Skeleton

                else if (Hit.gameObject.name == "SkeletonHit1")
                {
                    enemyBaseMain.hitconfirmed = 2;
                    animator.SetBool("BeingHit", true);
                }
                else if (Hit.gameObject.name == "SkeletonHit2")
                {
                    KnockBack(new Vector3(MinotaurHit1.x, MinotaurHit1.y, MinotaurHit1.z), Hit);
                }

                //Golem

                else if (Hit.gameObject.name == "GolemHit1")
                {
                    KnockBack(new Vector3(GolemHit1.x, GolemHit1.y, GolemHit1.z), Hit);
                }
                

                //all hits which don't have knockback
                else
                {
                    animator.SetBool("BeingHit", true);
                }
            }
        }

        int difficulty = PlayerPrefs.GetInt("Difficulty", 2);
        float damageMultiplier;

        if (difficulty <= 1)
        {
            damageMultiplier = 2;
        }
        else if (difficulty == 2)
        {
            damageMultiplier = 1;
        }
        else
        {
            damageMultiplier = 0.7f;
        }

        if (basemain.playerN == "Bot")
        {
            basemain.Health.HealthRemaining -= baseDamage * damageMultiplier;
        }
        else
        {
            basemain.Health.HealthRemaining -= baseDamage + bonusDamage;
        }
        
		Debug.Log ("hit!!!: " + Hit.gameObject.name);
        
    }

    void KnockBack(Vector3 knockBack, Collider hit)
    {
        //Debug.Log("rotation: "+ hit.gameObject.transform.rotation.y);
        if (hit.gameObject.transform.rotation.y == 0)
        {
            //Debug.Log("started "+ hit.gameObject.name);
            animator.SetBool("KnockBack", false);
            animator.SetBool("KnockBack", true);
            basemain.rotatedLeft = true;
            basemain.rotatedRight = false;
            KnockBackPass = new Vector3(knockBack.x, knockBack.y, knockBack.z);
            //rotate
            basemain.transform.rotation = Quaternion.Euler(0, -180, 0);
            basemain.shadow.transform.rotation = Quaternion.Euler(120, 0, 0);
            basemain.animator.gameObject.transform.rotation = Quaternion.Euler(30, 0, 0);
            basemain.animator.gameObject.GetComponent<SpriteRenderer>().flipX = true;
            //start movement
            basemain.movementV = KnockBackPass;
            basemain.gameObject.GetComponent<CharacterController>().Move(basemain.movementV * Time.deltaTime);
            Debug.Log("ended " + hit.gameObject.name);
        }
        else if (hit.gameObject.transform.rotation.y == -1)
        {
            animator.SetBool("KnockBack", false);
            animator.SetBool("KnockBack", true);
            basemain.rotatedLeft = false;
            basemain.rotatedRight = true;
            KnockBackPass = new Vector3(-knockBack.x, knockBack.y, knockBack.z);
            //rotate
            basemain.transform.rotation = Quaternion.Euler(0, 0, 0);
            basemain.shadow.transform.rotation = Quaternion.Euler(120, 0, 0);
            basemain.animator.gameObject.transform.rotation = Quaternion.Euler(30, 0, 0);
            basemain.animator.gameObject.GetComponent<SpriteRenderer>().flipX = false;
            //start movement
            basemain.movementV = KnockBackPass;
            basemain.gameObject.GetComponent<CharacterController>().Move(basemain.movementV * Time.deltaTime);
            Debug.Log("ended " + hit.gameObject.name);
        }
    }
}
