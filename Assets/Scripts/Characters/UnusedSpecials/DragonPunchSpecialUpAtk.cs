﻿using UnityEngine;
using System.Collections;

public class DragonPunchSpecialUpAtk : MonoBehaviour {

    protected BaseMain baseMain;

    public GameObject DragonPunchSpecialUpAtkHit;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
    }

    // Use this for initialization
    void OnEnable () {
        if (GetComponent<Transform>().rotation.y == 0)
        {
            baseMain.movementV.x = 2;
        }
        else if (GetComponent<Transform>().rotation.y == -1)
        {
            baseMain.movementV.x = -2;
        }
        baseMain.movementV.z = 0;
        baseMain.movementV.y = 6.5f;
        //baseMain.fallSpeed = 6.5f;
        GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);
        DragonPunchSpecialUpAtkHit.SetActive(true);
        baseMain.animator.SetBool("SpecialUpAtk", false);
    }
    void OnDisable()
    {
        baseMain.xairspeed = baseMain.movementV.x;
        baseMain.zairspeed = baseMain.movementV.z;
        DragonPunchSpecialUpAtkHit.SetActive(false);
    }

    // Update is called once per frame
    void FixedUpdate () {
        //baseMain.fallSpeed += Physics.gravity.y * Time.deltaTime;
        //baseMain.movementV.y = baseMain.fallSpeed;
        baseMain.movementV.y = baseMain.movementV.y + (Physics.gravity.y * Time.deltaTime);

        GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);
    }
}
