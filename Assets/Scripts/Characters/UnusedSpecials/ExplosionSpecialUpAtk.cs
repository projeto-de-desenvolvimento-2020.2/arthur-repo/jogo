﻿using UnityEngine;
using System.Collections;

public class ExplosionSpecialUpAtk : MonoBehaviour {

    protected BaseMain baseMain;

    public GameObject ExplosionSpecialUpAtkHit;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
    }

    // Update is called once per frame
    void OnEnable()
    {
        ExplosionSpecialUpAtkHit.SetActive(true);
        baseMain.animator.SetBool("SpecialUpAtk", false);
    }
    void OnDisable()
    {
        ExplosionSpecialUpAtkHit.SetActive(false);
    }
}
