﻿using UnityEngine;
using System.Collections;

public class BaseProjectileAttack1 : MonoBehaviour
{
    protected BaseMain baseMain;

    public GameObject StartingLocation;
    public GameObject Projectile;
    public AudioClip AttackSound;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
    }

    void OnEnable ()
    {
        baseMain.animator.SetBool("Attack", false);

        GameObject projectile = (GameObject)Instantiate(Projectile,
                                                      StartingLocation.transform.position,
                                                      Projectile.transform.rotation);
        projectile.layer = LayerMask.NameToLayer("Team" + baseMain.charaT + "Hit");
        if (baseMain.animator.gameObject.GetComponent<SpriteRenderer>().flipX)
        {
            projectile.GetComponent<Projectile>().Speed.x = -projectile.GetComponent<Projectile>().Speed.x;
        }

        //Hit.SetActive(true);
        if (AttackSound != null)
        {
            baseMain.audioSource.PlayOneShot(AttackSound);
        }
	}
    void Update()
    {
        if (baseMain.attack)
        {
            baseMain.animator.SetBool("Attack", true);
        }
    }
}
