﻿using UnityEngine;
using System.Collections;

public class BaseIdle : MonoBehaviour {

	protected BaseMain baseMain;

	protected float runTimeR = 0;
	protected float runTimeL = 0;
	protected float raycastRange;
	protected LayerMask isGroundMask;

	private void Awake()
	{
		baseMain = GetComponent<BaseMain>();
		raycastRange = baseMain.raycastRange;
		isGroundMask = baseMain.isGroundMask;
	}

	// Update is called once per frame
	void Update () 
	{
		//running
		if (baseMain.rightRun && runTimeR + 0.25 > Time.time && baseMain.rotatedRight)
		{
			baseMain.runningR = true;
			baseMain.animator.SetBool("Running", true);
		}
		if (baseMain.rightRun)
		{
			runTimeR = Time.time;
			runTimeL = 0;
		}
		if (baseMain.leftRun && runTimeL + 0.25 > Time.time && baseMain.rotatedLeft)
		{
			baseMain.animator.SetBool("Running", true);
			baseMain.runningL = true;
		}
		if (baseMain.leftRun)
		{
			runTimeL = Time.time;
			runTimeR = 0;
		}

		//walking animation
		if (baseMain.left && !baseMain.right || baseMain.right && !baseMain.left || baseMain.up && !baseMain.down || baseMain.down && !baseMain.up) {
			baseMain.animator.SetBool("Walking", true);
		}
		//if (!left || right && !right || left && !up || down && !down || up) 
		else{
			baseMain.animator.SetBool("Walking", false);
		}

		//walking
		if (baseMain.right && !baseMain.left) {
			baseMain.movementV.x = baseMain.movement.xspeed *  1;
		} else if (baseMain.left && !baseMain.right) {
			baseMain.movementV.x = baseMain.movement.xspeed * -1;
		} else {
			baseMain.movementV.x = 0;
		}
		if (baseMain.up && !baseMain.down) {
			baseMain.movementV.z = baseMain.movement.zspeed *  1;
		} else if (baseMain.down && !baseMain.up) {
			baseMain.movementV.z = baseMain.movement.zspeed * -1;
		} else {
			baseMain.movementV.z = 0;
		}
        //special
        if (baseMain.special)
        {
            //attack
            if (baseMain.Mana.ManaRemaining >= baseMain.Mana.SpecialCost)
            {
				baseMain.Mana.ManaRemaining -= baseMain.Mana.SpecialCost;
				baseMain.animator.SetBool("SpecialUpAtk", true);
            }
        }
        //base actions
        else
        {
            //jumping
            if (baseMain.jump)
            {
                baseMain.xairspeed = baseMain.movementV.x;
                baseMain.zairspeed = baseMain.movementV.z;
                baseMain.movementV.y = baseMain.movement.jumpPower;
                //baseMain.fallSpeed = baseMain.movement.jumpPower;

				Vector3 moveDirection = new Vector3(baseMain.movementV.x, baseMain.movementV.y, baseMain.movementV.z);
				//moveDirection.y -= gravity * Time.deltaTime;

				GetComponent<CharacterController>().Move(moveDirection * Time.deltaTime);

			}
            //defend
            if (baseMain.defend)
            {
                baseMain.animator.SetBool("Defend", true);
            }
            //attack
            if (baseMain.attack)
            {
                baseMain.animator.SetBool("Attack", true);
            }
        }
	}
	void FixedUpdate (){
		if (!IsGrounded()) {		//check fall
			baseMain.animator.SetBool("JumpAir", true);
		}

		//rotate
		if (baseMain.rotatedRight && !baseMain.rotatedLeft)
		{
			transform.rotation = Quaternion.Euler(0, 0, 0);
			baseMain.shadow.transform.rotation = Quaternion.Euler(120, 0, 0);
			baseMain.animator.gameObject.transform.rotation = Quaternion.Euler(30,0,0);
			baseMain.animator.gameObject.GetComponent<SpriteRenderer>().flipX = false;
		}
		else if (baseMain.rotatedLeft && !baseMain.rotatedRight)
		{
			transform.rotation = Quaternion.Euler(0, -180, 0);
			baseMain.shadow.transform.rotation = Quaternion.Euler(120, 0, 0);
			baseMain.animator.gameObject.transform.rotation = Quaternion.Euler(30, 0, 0);
			baseMain.animator.gameObject.GetComponent<SpriteRenderer>().flipX = true;
		}



		GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);
	}
	bool IsGrounded () //verify if character is touching the ground
	{
		return Physics.Raycast (transform.position, - Vector3.up, raycastRange, isGroundMask);
	}
}
