﻿using UnityEngine;
using System.Collections;

public class BaseDefend : MonoBehaviour {

	protected BaseMain baseMain;

	private void Awake()
	{
		baseMain = GetComponent<BaseMain>();
	}

	// Update is called once per frame
	void OnEnable () {
		baseMain.animator.SetBool("Defend", false);
	}
}
