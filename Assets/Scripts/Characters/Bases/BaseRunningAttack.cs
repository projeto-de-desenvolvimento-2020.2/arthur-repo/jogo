﻿using UnityEngine;
using System.Collections;

public class BaseRunningAttack : MonoBehaviour {

    protected BaseMain baseMain;

    public GameObject RunHit;
    public AudioClip AttackSound;

    public float speedPercentLoss = 0;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
    }

    void OnEnable()
    {
        baseMain.animator.SetBool("Attack", false);
        RunHit.SetActive(true);
        if (AttackSound != null)
        {
            baseMain.audioSource.PlayOneShot(AttackSound);
        }
    }
    void OnDisable()
    {
        RunHit.SetActive(false);
        baseMain.runningL = false;
        baseMain.runningR = false;
        baseMain.animator.SetBool("Running", false);
    }
    void FixedUpdate()
    {
        baseMain.movementV = baseMain.movementV * (1 - speedPercentLoss);
        GetComponent<CharacterController>().Move(baseMain.movementV * Time.deltaTime);
    }
}
