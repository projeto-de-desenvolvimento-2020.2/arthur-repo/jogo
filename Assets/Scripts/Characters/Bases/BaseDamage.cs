﻿using UnityEngine;
using System.Collections;

public class BaseDamage : MonoBehaviour {

	protected BaseMain baseMain;

	private void Awake()
	{
		baseMain = GetComponent<BaseMain>();
	}

	// Use this for initialization
	void OnEnable () 
	{
		baseMain.animator.SetBool("BeingHit", false);
	}
}
