﻿using UnityEngine;
using System.Collections;

public class BasePreAttack1 : MonoBehaviour {

    protected BaseMain baseMain;
    public AudioClip AttackSound;
    public bool armored = false;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
    }

    void OnEnable ()
    {
        baseMain.specialStatus.armored = armored;
        if (AttackSound != null)
        {
            baseMain.audioSource.PlayOneShot(AttackSound);
        }
    }

}
