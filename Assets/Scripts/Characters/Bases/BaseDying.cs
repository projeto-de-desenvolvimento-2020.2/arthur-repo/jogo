﻿using UnityEngine;
using System.Collections;

public class BaseDying : MonoBehaviour {

    protected BaseMain baseMain;

    public GameObject Hurtbox;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
    }

    void OnEnable ()
    {
        Hurtbox.SetActive(false);
	}

}
