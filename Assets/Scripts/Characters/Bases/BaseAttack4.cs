﻿using UnityEngine;
using System.Collections;

public class BaseAttack4 : MonoBehaviour {

    protected BaseMain baseMain;

    public GameObject Hit;
    public AudioClip AttackSound;

    private void Awake()
    {
        baseMain = GetComponent<BaseMain>();
    }

    void OnEnable()
    {
        baseMain.animator.SetBool("Attack", false);
        Hit.SetActive(true);
        if (AttackSound != null)
        {
            baseMain.audioSource.PlayOneShot(AttackSound);
        }
    }
    void Update()
    {
        if (baseMain.attack)
        {
            baseMain.animator.SetBool("Attack", true);
        }
    }
    void OnDisable()
    {
        Hit.SetActive(false);
    }
}
