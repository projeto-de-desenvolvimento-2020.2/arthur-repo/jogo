﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Selector : MonoBehaviour {

	public int[] selected = {0, 0, 0, 0,  0, 0, 0, 0};
    public GameObject[] selectors;

    public GameObject SelectPlayerN;
    public GameObject SelectChar;

    protected bool selecting = false;
    // Use this for initialization
    void Start () 
	{
        selected = new int[GameObject.FindGameObjectsWithTag("SelectorUI").Length];
		DontDestroyOnLoad(transform.gameObject);
	}
	
	// Update is called once per frame
	void Update () 
	{
        if(SceneManager.GetActiveScene().name == "MainMenu")
        {
            Destroy(gameObject);
        }
        else if (selecting)
        {
            selectors = GameObject.FindGameObjectsWithTag("SelectorUI");
            if (selectors.Length == 0)
            {
                selecting = false;
                SceneManager.LoadScene("Stage 1");
            }
        }
	}

    public void SelectorSpawner(bool twoPlayers)
    {
        SelectChar.SetActive(true);
        if (twoPlayers)
        {
            SelectChar.transform.Find("SelectorP2").gameObject.SetActive(true);
            SelectChar.transform.Find("RightBannerParts").gameObject.SetActive(true);
        }
        selecting = true;
        SelectPlayerN.SetActive(false);
        selected = new int[GameObject.FindGameObjectsWithTag("SelectorUI").Length];
    }
}
