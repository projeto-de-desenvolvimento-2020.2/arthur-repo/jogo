﻿using UnityEngine;
using System.Collections;

public class MainMenuQuit : MonoBehaviour {

	public AudioSource audioSource;
	public AudioClip UISound;

	public void quit () {
		audioSource.PlayOneShot(UISound);
		Application.Quit();
	}
}
