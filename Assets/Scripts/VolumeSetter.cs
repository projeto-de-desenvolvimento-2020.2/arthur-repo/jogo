using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeSetter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int volume = PlayerPrefs.GetInt("Volume", 100);
        AudioSource[] audioSources = FindObjectsOfType(typeof(AudioSource), true) as AudioSource[];
        foreach (AudioSource source in audioSources)
        {
            source.volume = volume / 100f;       
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
